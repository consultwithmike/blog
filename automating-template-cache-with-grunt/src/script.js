var app = angular.module('app', ['templates-main']);

app.controller('MainCtrl', function MainCtrl() {
  var vm = this;
  vm.title = 'Application Title';
});

app.directive('appTemplate', function appTemplate() {
  return {
    restrict: 'E',
    templateUrl: 'app.tpl.html'
  };
});

app.directive('moduleTemplate', function moduleTemplate() {
  return {
    restrict: 'E',
    templateUrl: 'module/module.tpl.html'
  };
});