angular.module('templates-main', ['app.tpl.html', 'module/module.tpl.html']);

angular.module("app.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("app.tpl.html",
    "<h2>This is my compiled template!</h2>\n" +
    "<module-template></module-template>");
}]);

angular.module("module/module.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("module/module.tpl.html",
    "<h3>This is my module template.</h3>");
}]);
