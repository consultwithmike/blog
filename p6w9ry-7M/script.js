var app = angular.module('app', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/app.tpl.html',
      controller: 'TaskController as vm'
    });

  $urlRouterProvider.otherwise('/');
});

// app.factory('Task', function($http) {
//   return {
//     getAll: function(cb) {
//       $http.get('/tasks')
//         .then(function(res) {
//           if (cb) {
//             cb(res.data);
//           }
//         }, function(err) {
//           alert(err);
//         });
//     },
//     insert: function(task, cb) {
//       $http.post('/tasks', task)
//         .then(function(res) {
//           if (cb) {
//             cb(res.data);
//           }
//         }, function(err) {
//           if (cb) {
//             cb(task);
//           }
//           alert(err);
//         });
//     },
//     update: function(task, cb) {
//       $http.put('/tasks', task)
//         .then(function(res) {
//           if (cb) {
//             cb(res.data);
//           }
//         }, function(err) {
//           if (cb) {
//             cb(task);
//           }
//           alert(err);
//         });
//     }
//   };
// });

app.resource('Task', function($resource) {
  return $resource('/tasks', {}, {
    'update': { method: 'PUT' }
  });
});

app.directive('task', function() {
  return {
    restrict: "E",
    templateUrl: 'templates/task.tpl.html',
    scope: {
      task: '=',
      updateTask: '&'
    }
  };
});

app.controller('TaskController', function(Task) {
  var vm = this;
  var newId = -1;
  
  // Task.getAll(function(data) {
  //   vm.tasks = data;
  // })
  
  vm.tasks = Task.query();
  
  // vm.newTask = function() {
  //   vm.tasks.push({id: newId});
  //   newId--;
  // };
  
  vm.newTask = function() {
    var task = new Task();
    task.id = newId;
    vm.tasks.push(task);
    newId--;
  };

  // vm.updateTask = function(task) {
  //   if (task.id < 0) {
  //     Task.insert(task, function(res) {
  //       var idx = _.indexOf(vm.tasks, _.find(vm.tasks, {id: task.id}));
  //       _.merge(task, res);
  //       vm.tasks[idx] = task;
  //     });
  //   } else {
  //     Task.update(task);
  //   }
  // };

  vm.updateTask = function(task) {
    if (task.id < 0) {
      task.$save(function(res) {
        var idx = _.indexOf(vm.tasks, _.find(vm.tasks, {id: task.id}));
        _.merge(task, res);
        vm.tasks[idx] = task;
      });
    } else {
      task.update();
    }
  };
});