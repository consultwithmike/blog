(function() {
  var _ = require('lodash');

  var defaultTasks = [
    {id: 1, name: 'Task 1', complete: true},
    {id: 2, name: 'Task 2'},
    {id: 3, name: 'Task 3'}
  ];
  var tasks = defaultTasks.slice(0);

  module.exports = function(app) {
    app.get('/tasks', function(req, res) {
      res.json(tasks);
    });
    
    app.post('/tasks', function(req, res) {
      if (!req.body) {
        res.status(400).end('Missing the new task to add.');
      } else if (!req.body.name) {
        res.status(400).end('Missing the task name.');
      } else {
        req.body.id = tasks.length + 1;
        tasks.push(req.body);
        
        res.status(201).json(req.body);
      }
    });
    
    app.put('/tasks', function(req, res) {
      if (!req.body.id && !req.body.name) {
        tasks = defaultTasks.slice(0);
        res.json(tasks);
      } else if (!req.body.id) {
        res.status(400).end('Missing the task id.');
      } else if (!req.body.name) {
        res.status(400).end('Missing the task name.');
      } else {
        var taskIndex = _.indexOf(tasks, _.find(tasks, {id: req.body.id}));
        if (taskIndex < 0) {
          res.status(404).end('Task not found for id: ' + req.body.id);
        }
        
        tasks[taskIndex] = req.body;
        res.json(tasks[taskIndex]);
      }
    });
  };
})();