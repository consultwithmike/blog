var app = angular.module('app', ['ui.router'], function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('invoice', {
      url: '/',
      templateUrl: 'templates/invoice.tpl.html'
    })
    .state('invoice.items', {
      url: 'items',
      views: {
        'invoice-detail-view': {
          templateUrl: 'templates/items.tpl.html'
        }
      }
    })
    .state('invoice.shipping-details', {
      url: 'shipping-details',
      views: {
        'invoice-detail-view': {
          templateUrl: 'templates/shipping-details.tpl.html'
        }
      }
    })
    .state('invoice.items.bom', {
      url: '/:id/bom',
      views: {
        'invoice-detail-view@invoice': {
          templateUrl: 'templates/bom.tpl.html',
          controller: 'BomController as vm'
        }
      },
      resolve: {
        id: function($stateParams) {
          return parseInt($stateParams.id);
        }
      }
    });

  $urlRouterProvider.otherwise('/');
});

app.controller('BomController', function(id) {
  var vm = this;
  vm.model = buildModel(id);

  function buildModel(id) {
    return {
        heading: 'Bill of Material for Item #' + id,
        description: 'Some item description for item #' + id,
        items: [
          { name: 'Sub Item ' + id + '.1' },
          { name: 'Sub Item ' + id + '.2' }
        ]
      };
  }
});